"use client";
import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import { EditNote, Print, Pages, Report, AdsClick } from '@mui/icons-material';
import Typography from '@mui/material/Typography';

export default function OrderDetailActions() {
    return (
        <Card>
            <CardContent>
                <Grid 
                    container
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="flex-start"
                >
                    <Grid item xs={12} sm={12} md={2}>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#00704B",textDecoration:"underline"}}>
                            <EditNote/> Edit Order Details
                        </Typography>
                    </Grid>
                    
                    <Grid item xs={12} sm={12} md={2}>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#00704B",textDecoration:"underline"}}>
                            <Print/> Print Order Details
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={12} md={3}>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#00704B",textDecoration:"underline"}}>
                            <Pages/> Change Delivery Address
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={12} md={2}>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#00704B",textDecoration:"underline"}}>
                            <Report/> Reports
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={12} md={2}>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#00704B",textDecoration:"underline"}}>
                            <AdsClick/> Actions
                        </Typography>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>

    )
}