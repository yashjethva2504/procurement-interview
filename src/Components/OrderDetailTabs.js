"use client";
import * as React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import { LocationOn, Call, Phone, Mail, Person, AttachFile } from '@mui/icons-material';
import { PieChart } from '@mui/x-charts/PieChart';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';
import Divider from '@mui/material/Divider';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
export default function OrderDetailTabs() {

    const data = [
        { label: 'Group A', value: 400 },
        { label: 'Group B', value: 300 },
        { label: 'Group C', value: 300 },
        { label: 'Group D', value: 200 },
        { label: 'Group E', value: 278 },
        { label: 'Group F', value: 189 },
    ];

    return (
        <>
        <Card>
            <CardContent>
                <Tabs
                    value={"one"} 
                    aria-label="wrapped label tabs example"
                    
                    >
                    <Tab
                        value="one"
                        label="Summary"
                        sx={{width:"10%",fontSize:"10px"}}    
                        disableRipple 
                    />
                    <Tab sx={{width:"10%",fontSize:"10px"}} value="two" label="Order Lines" />
                    <Tab sx={{width:"10%",fontSize:"10px"}} value="three" label="Supplier" />
                    <Tab sx={{width:"10%",fontSize:"10px"}} value="three" label="Analysis" />
                    <Tab sx={{width:"10%",fontSize:"10px"}} value="three" label="Freight Details" />
                    <Tab sx={{width:"10%",fontSize:"10px"}} value="three" label="Delivery" />
                    <Tab sx={{width:"10%",fontSize:"10px"}} value="three" label="Invoice" />
                    <Tab sx={{width:"10%",fontSize:"10px"}} value="three" label="Memos" />
                    <Tab sx={{width:"10%",fontSize:"10px"}} value="three" label="Notes & Attachment" />
                    <Tab sx={{width:"10%",fontSize:"10px"}} value="three" label="Budgets" />
                </Tabs>
                <Divider sx={{backgroundColor: 'grey',}} />
            </CardContent>
        </Card>
        <Card 
        sx ={{
                overflowY: "scroll",
                height: "400px"
            }}>
            <CardContent>
                <Card sx={{backgroundColor:"#EFF1F1"}}>
                    <CardContent>
                        <Typography variant="body" sx={{display:"flex",fontWeight:700}}>
                            COMPONENTS
                        </Typography>
                        <Typography variant="caption" sx={{display:"flex"}}>
                            No items found
                        </Typography>
                    </CardContent>
                </Card>
                <Card sx={{backgroundColor:"#EFF1F1",marginTop:"2%"}}>
                    <CardContent>
                        <Typography variant="body" sx={{display:"flex",fontWeight:700}}>
                            AUTHORIZED SUPPLIER
                        </Typography>
                        <Grid container>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start"}}>
                                    Date Authorized
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start"}}>
                                    Authorized By 
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start"}}>
                                    Invoice Chase Date
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start"}}>
                                    PO Chase Date
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start"}}>
                                    PO Ref No.
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start"}}>
                                    Account Code
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start",fontWeight:700}}>
                                    10 Jul 2019
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start",fontWeight:700}}>
                                    Jim Darren
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start",fontWeight:700}}>
                                    -
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start",fontWeight:700}}>
                                    -
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start",fontWeight:700}}>
                                    -
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start",fontWeight:700}}>
                                    -
                                </Typography>
                            </Grid>
                            
                        </Grid>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#2a6cc1",textDecoration:"underline",marginTop:"15px"}}>
                            Repaires (Riding Squards / Manpower)
                        </Typography>

                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#00704B",textDecoration:"underline",marginTop:"5px"}}>
                            <LocationOn/> 1st Floor, Skypark, 8 Elliot Place
                        </Typography>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#00704B",textDecoration:"underline",marginTop:"5px"}}>
                            <Call/> +0 44 (0) 141 405 1299
                        </Typography>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#00704B",textDecoration:"underline",marginTop:"5px"}}>
                            <Phone/> +0 44 (0) 141 405 1299
                        </Typography>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#00704B",textDecoration:"underline",marginTop:"5px"}}>
                            <Mail/> repairs@services.com
                        </Typography>
                    </CardContent>
                </Card>

                <Card sx={{backgroundColor:"#EFF1F1",marginTop:"2%"}}>
                    <CardContent>
                        <Typography variant="body" sx={{display:"flex",fontWeight:700}}>
                            SUPPLIERS
                        </Typography>
                        <TableContainer>
                            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                                <TableHead>
                                <TableRow>
                                    <TableCell >Status</TableCell >
                                    <TableCell align="right">Supplier Name</TableCell>
                                    <TableCell align="right">Goods</TableCell>
                                    <TableCell align="right">Freight</TableCell>
                                    <TableCell align="right">Total</TableCell>
                                    <TableCell align="right">Cur</TableCell>
                                    <TableCell align="right">Base (USD)</TableCell>
                                </TableRow>
                                </TableHead>
                                <TableBody>
                                
                                    <TableRow >
                                        <TableCell component="th" scope="row">
                                            <Chip label="YJ" color="success" />
                                        </TableCell>
                                        <TableCell align="right" ><Typography variant="caption">Engineering Co. Ltd</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">3,456.90</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">0.00</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">3,456.90</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">EUR</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">3,811.90</Typography></TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            <Chip label="YJ" color="success" />
                                        </TableCell>
                                        <TableCell align="right" ><Typography variant="caption">Engineering Co. Ltd</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">3,456.90</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">0.00</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">3,456.90</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">EUR</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">3,811.90</Typography></TableCell>
                                    </TableRow>
                                
                                </TableBody>
                            </Table>
                            </TableContainer>
                    </CardContent>
                </Card>

                <Card sx={{backgroundColor:"#EFF1F1",marginTop:"2%"}}>
                    <CardContent>
                        <Typography variant="body" sx={{display:"flex",fontWeight:700}}>
                            COST
                        </Typography>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={12} md={6}>
                                <Card sx={{marginTop:"2%"}}>
                                    <CardHeader title="Goods / Service Balance" />
                                    <CardContent>
                                        <PieChart
                                            series={[
                                                {
                                                startAngle: -90,
                                                endAngle: 90,
                                                data,
                                                },
                                            ]}
                                            height={300}
                                        />
                                    </CardContent>
                                </Card>
                            </Grid>
                            <Grid item xs={12} sm={12} md={6}>
                                <Card sx={{marginTop:"2%"}}>
                                    <CardHeader title="Freight / Delivery Balance" />
                                    <CardContent>
                                        <PieChart
                                            series={[
                                                {
                                                startAngle: -90,
                                                endAngle: 90,
                                                data,
                                                },
                                            ]}
                                            height={300}
                                        />
                                    </CardContent>
                                </Card>
                            </Grid>
                        </Grid>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,marginTop:"2%"}}>
                            Total PO Amount  21643128736
                            
                        </Typography>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700}}>
                            Total Outstanding Amount  23223
                            
                        </Typography>
                    </CardContent>
                </Card>
                <Card sx={{backgroundColor:"#EFF1F1",marginTop:"2%"}}>
                    <CardContent>
                        <Typography variant="body" sx={{display:"flex",fontWeight:700}}>
                            INVOICES
                        </Typography>
                        <TableContainer>
                            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                                <TableHead>
                                <TableRow>
                                    <TableCell >Attachment</TableCell >
                                    <TableCell >Status</TableCell >
                                    <TableCell align="right">Reference</TableCell>
                                    <TableCell align="right">Amount</TableCell>
                                    <TableCell align="right">Currency</TableCell>
                                    <TableCell align="right">Invoice date</TableCell>
                                    <TableCell align="right">Paid date</TableCell>
                                </TableRow>
                                </TableHead>
                                <TableBody>
                                
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            <AttachFile />
                                        </TableCell>
                                        <TableCell align="right"><Typography variant="caption">Invoice Paid</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">PJJD3737-SRIN</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">24,484</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">USD</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">23 Sep 2019</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">30 Jun 2020</Typography></TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            <AttachFile />
                                        </TableCell>
                                        <TableCell align="right"><Typography variant="caption">Invoice Paid</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">PJJD3737-SRIN</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">24,484</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">USD</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">23 Sep 2019</Typography></TableCell>
                                        <TableCell align="right"><Typography variant="caption">30 Jun 2020</Typography></TableCell>
                                    </TableRow>
                                
                                </TableBody>
                            </Table>
                            </TableContainer>
                    </CardContent>
                </Card>
                <Card sx={{backgroundColor:"#EFF1F1",marginTop:"2%"}}>
                    <CardContent>
                        <Typography variant="body" sx={{display:"flex",fontWeight:700}}>
                            RECEIVED AT
                        </Typography>
                        <Grid container>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start"}}>
                                    Ordered date
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start"}}>
                                    Received On 
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start"}}>
                                    Port
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start",fontWeight:700}}>
                                    10 Jul 2019
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start",fontWeight:700}}>
                                    10 Jul 2019
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={2}>
                                <Typography variant="caption" sx={{display: "flex",justifyContent: "start",fontWeight:700}}>
                                    Dover
                                </Typography>
                            </Grid>
                            
                            
                        </Grid>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#2a6cc1",textDecoration:"underline",marginTop:"15px"}}>
                            <Person/> George Vessel berth
                        </Typography>

                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#00704B",textDecoration:"underline",marginTop:"5px"}}>
                            <LocationOn/> 1st Floor, Skypark, 8 Elliot Place
                        </Typography>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#00704B",textDecoration:"underline",marginTop:"5px"}}>
                            <Call/> +0 44 (0) 141 405 1299
                        </Typography>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#00704B",textDecoration:"underline",marginTop:"5px"}}>
                            <Phone/> +0 44 (0) 141 405 1299
                        </Typography>
                        <Typography variant="caption" sx={{display:"flex",fontWeight:700,color:"#00704B",textDecoration:"underline",marginTop:"5px"}}>
                            <Mail/> repairs@services.com
                        </Typography>
                    </CardContent>
                </Card>
            </CardContent>
        </Card>
        </>

    )
}