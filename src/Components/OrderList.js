import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import { SailingOutlined, PhishingOutlined } from '@mui/icons-material';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';

export default function OrderList() {
    return (

        
        <Grid
            container
            sx={{marginTop:"5%",}}>
           
            <Grid item xs={12} sm={12} md={12}>
                ORDERS
            </Grid>
            <Grid item xs={12} sm={12} md={12} 
            style ={{
                overflowY: "scroll",
                height: "500px"
            }}>
                {
                    new Array(5).fill(0).map(() => (
                        <Card sx={{marginTop:"5%",backgroundColor:"#E8ECED"}} >
                            <CardContent>
                                <Grid container >
                                    <Grid item xs={6}>
                                        <Typography variant="subtitle2" gutterBottom sx={{fontWeight:700}}>
                                            4907 - 12345
                                        </Typography>
                                        
                                    </Grid>
                                    <Grid item xs={6} sx={{display: "flex",justifyContent: "end"}}>
                                        <PhishingOutlined/>
                                        <Chip label="YJ" color="success" />
                                    </Grid>
                                    
                                </Grid>
                                <Typography variant="subtitle2" gutterBottom sx={{fontWeight:700}}>
                                    HOTEL/TECH WORK
                                </Typography>
                                <Grid container spacing={12}>
                                    <Grid item xs={6}>
                                        <Button variant="contained" size="small" disableRipple startIcon={<SailingOutlined sx={{color:"black"}}/>} sx={{backgroundColor:'#80E7FF'}} >
                                            <Typography variant="subtitle2" sx={{color:"black",textTransform: 'none'}}>Lorem Ipsum</Typography>
                                        </Button>
                                    </Grid>
                                    <Grid item xs={6}
                                    direction="row"
                                    justifyContent="flex-end"
                                    >
                                        <Typography variant="caption" sx={{display: "flex",justifyContent: "end"}}>
                                            Services <br/>
                                            12 Jul 1993
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </Card>
                    ))
                }
                
                
            </Grid>
        </Grid>
        

    )
}