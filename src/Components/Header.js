import * as React from 'react';
import Grid from '@mui/material/Grid';
import { SearchOutlined, NotificationsActiveOutlined, AppsOutlined } from '@mui/icons-material';

export default function Header() {
  return (
    <Grid container>
      <Grid item xs={8}>
        <p>Procurement</p>
      </Grid>
      <Grid item xs={4} sx={{alignItems: "center",display: "flex",justifyContent:"end"}}> 
        <SearchOutlined/>
        <NotificationsActiveOutlined/>
        <AppsOutlined/>
      </Grid>
      
    </Grid>
  )
}