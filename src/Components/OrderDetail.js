"use client";
import * as React from 'react';
import Divider from '@mui/material/Divider';
import OrderDetailHeader from '../Components/OrderDetailHeader';
import OrderDetailActions from '../Components/OrderDetailActions';
import OrderDetailTabs from '../Components/OrderDetailTabs';
export default function OrderDetail() {

    return (
        <>
            <OrderDetailHeader/>
            <Divider light />
            <OrderDetailActions/>
            <Divider light />
            <OrderDetailTabs/>
        </>

    )
}