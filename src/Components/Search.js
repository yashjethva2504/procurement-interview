import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Input from '@mui/material/Input';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import Grid from '@mui/material/Grid';
import { SearchOutlined, SailingOutlined, SettingsApplications} from '@mui/icons-material';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

export default function Search() {
  return (
    
    <Card sx={{backgroundColor:"#F2EEEB"}}>
      <CardContent>
        <Grid container>
            <Grid item xs={6}>
            <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
                <Input
                id="input-with-icon-adornment"
                placeholder='My Responsibilities'
                startAdornment={
                    <>
                        <InputAdornment position="start">
                            <SailingOutlined />
                        </InputAdornment>
                        
                    </>
                }
                endAdornment={
                    <>
                        <InputAdornment position="end">
                            <SearchOutlined />
                        </InputAdornment>
                    </>
                }
                
                />
            </FormControl>
            </Grid>
            <Grid item xs={5}> 
                <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
                
                    <Select
                        displayEmpty
                        inputProps={{ 'aria-label': 'Without label' }}
                        >
                        <MenuItem value="">
                            <em>PO</em>
                        </MenuItem>
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                </FormControl>
            </Grid>
            <Grid item xs={1} sx={{alignItems: "center",display: "flex",justifyContent:"end"}}> 
                <SettingsApplications/>
            </Grid>
        </Grid>
        <Grid container spacing={2}>
            <Grid item xs={6}>
              <TextField
                    id="coy-id"
                    label="Coy Id"
                    placeholder="Enter Coy Id"
                    variant="standard"
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
            </Grid>
            <Grid item xs={6}>
                <TextField
                    id="standard-number"
                    label="Order No"
                    placeholder="Enter Order No"
                    type="text"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    variant="standard"
                />
            </Grid>
        </Grid>

        <Grid container spacing={2}>
            <Grid item xs={2}>
                <Typography variant="caption" sx={{fontWeight:700,textDecoration:"underline"}}>
                    Sort
                </Typography>
            </Grid>
            <Grid item xs={2}/>
            <Grid item xs={2}>
                <Typography variant="caption" sx={{fontWeight:700,textDecoration:"underline"}}>
                    Group By
                </Typography>
            </Grid>
            <Grid item xs={2}>
                <Typography variant="caption" sx={{fontWeight:700,textDecoration:"underline"}}>
                    Clear
                </Typography>
            </Grid>
            <Grid item xs={2}/>
            <Grid item xs={2}>
                <Typography variant="caption" sx={{fontWeight:700,textDecoration:"underline"}}>
                    Search
                </Typography>
            </Grid>
        </Grid>
        
      </CardContent>
      
    </Card>
    
  )
}