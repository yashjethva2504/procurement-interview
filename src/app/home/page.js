import * as React from 'react';
import Header from '../../Components/Header';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Search from '../../Components/Search';
import OrderList from '../../Components/OrderList';
import OrderDetail from '../../Components/OrderDetail';

export default function Home() {
  return (
    <Container maxWidth={false}>
      <Header/>
      <Grid container spacing={2}>
        <Grid item xs={12} md={4}>
          <Search/>
          <OrderList/>
        </Grid>
        <Grid item xs={12} md={8} > 
          <OrderDetail/>
        </Grid>
        
      </Grid>
    </Container>
  )
    
  
    
}